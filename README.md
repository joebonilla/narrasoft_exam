Created in Laravel 8 Framework

Packages:
* Jetstream
* Inertia

Frontend:
* VueJS

Installation:
* Create a database any name.
* Go to root directory and copy and paste .env.example to .env.
* Change database name and credentials.
* Run `composer install && npm install && npm run dev`
* Run `php artisan migrate --seed`
* Run `php artisan serve` and go to provided url
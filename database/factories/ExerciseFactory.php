<?php

namespace Database\Factories;

use App\Models\Exercise;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ExerciseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Exercise::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->randomElement(['Pull-ups', 'Push-ups', 'Dips', 'Squats', 'Rows']),
            'goal' => '10',
            'current' => $this->faker->numberBetween($min = 0, $max = 20)
        ];
    }
}

<?php

namespace Database\Seeders;

use App\Models\Entry;
use App\Models\Exercise;
use App\Models\User;
use App\Models\Workout;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(100)->create();
        Workout::factory(100)->create();
        Exercise::factory(100)->create();
        Entry::factory(100)->create();
    }
}

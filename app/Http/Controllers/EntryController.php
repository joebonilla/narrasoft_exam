<?php

namespace App\Http\Controllers;

use App\Models\Entry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class EntryController extends Controller
{
    public function index()
    {
        return Inertia::render('Dashboard', [
            'entries' => $this->getAllEntries()
        ]);
    }

    public function getAllEntries()
    {
        $data = DB::table('entries')
            ->select([
                "users.name as user_name",
                "workouts.id as id",
                "workouts.workout_date as workout_date",
                "exercises.name as exercise_name",
                "exercises.goal as goal",
                "exercises.current as current",
                DB::raw("CONCAT(round((current/goal) * 100),'%') as percentage")
            ])
            ->leftJoin('workouts', 'workouts.id', '=', 'entries.workout_id')
            ->leftJoin('users', 'users.id', '=', 'entries.user_id')
            ->leftJoin('exercises', 'exercises.id', '=', 'entries.exercise_id')
            ->orderByDesc('id')
            ->paginate();

        return $data;

    }
}
